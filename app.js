const PORT = 8080;

const http = require('http');
const express = require('express');
const path = require("path");
const app = express();
const bodyParser = require('body-parser');
app.engine('pug', require('pug').__express)

app.set('views', __dirname + '/frontend/views/pug');
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, "/")));
app.use(bodyParser.json({limit: '10mb'}));

app.locals.basedir = __dirname;

app.get("/index", function (req, res)
{
    res.render("index");
});

app.get("/", function (req, res)
{
    res.render("index");
});

http.createServer(app).listen(PORT, function(){
    console.log("Express server listening on port " + PORT);
    console.log("http://localhost:" + PORT);
});
